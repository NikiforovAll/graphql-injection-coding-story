using GraphQLInjectionCodingStory;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddRouting();

var graphqlExecutor = builder.Services.AddGraphQLServer()
    .AddQueryType<Query>();

if (builder.Environment.IsProduction())
{
    graphqlExecutor
        .AddValidationVisitor<NoIntrospectionDocumentValidatorVisitor>();
}

builder.Services.AddTransient<IDatabaseConnectionFactory>(e =>
    new SqlConnectionFactory(
        builder.Configuration.GetConnectionString("DefaultConnection"))
);

var app = builder.Build();

if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app
    .UseRouting()
    .UseEndpoints(endpoint => endpoint.MapGraphQL());

app.Run();
