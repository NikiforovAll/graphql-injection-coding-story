namespace GraphQLInjectionCodingStory;

using System.Data;
using System.Threading.Tasks;
using Npgsql;

public interface IDatabaseConnectionFactory
{
    Task<IDbConnection> CreateConnectionAsync();
}

public class SqlConnectionFactory : IDatabaseConnectionFactory
{
    private readonly string _connectionString;

    public SqlConnectionFactory(string connectionString) =>
        _connectionString = connectionString
            ?? throw new ArgumentNullException(nameof(connectionString));

    public async Task<IDbConnection> CreateConnectionAsync()
    {
        var sqlConnection = new NpgsqlConnection(_connectionString);
        await sqlConnection.OpenAsync();
        return sqlConnection;
    }
}
