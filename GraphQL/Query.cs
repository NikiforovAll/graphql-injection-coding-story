namespace GraphQLInjectionCodingStory;

using Dapper;
using HotChocolate;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
public class Query
{
    public async Task<IEnumerable<OrderDto>> GetOrders([Service] IDatabaseConnectionFactory db)
    {
        var conn = await db.CreateConnectionAsync();
        var orders = await conn.QueryAsync<OrderDto>(
            "SELECT * FROM \"Orders\"");

        return orders;
    }

    public async Task<OrderDto> GetOrder([Service] IDatabaseConnectionFactory db, int id)
    {
        var conn = await db.CreateConnectionAsync();
        var order = await conn.QueryFirstAsync<OrderDto>(
            "SELECT * FROM \"Orders\" LIMIT 1");

        return order;
    }

    public async Task<IEnumerable<OrderDto>> SearchOrder(
        [Service] IDatabaseConnectionFactory db,
        string sku)
    {
        var conn = await db.CreateConnectionAsync();
        var dynamicParameters = new DynamicParameters();
        dynamicParameters.Add("@Sku", $"%{sku}%");

        using var multi = await conn.QueryMultipleAsync(GetSearchOrderTerm(), dynamicParameters);
        var orders = multi.Read<OrderDto>();
        var totalOrders = multi.Read<string>().First();

        foreach (var order in orders)
        {
            order.TotalOrdersLabel = $"Total orders in the system: {totalOrders}";
        }

        return orders;

        string GetSearchOrderTerm()
        {
            var searchTerm = $"SELECT * FROM \"Orders\" WHERE \"Sku\" LIKE @Sku";
            var totalOrdersTerm = "SELECT COUNT(*) FROM \"Orders\"";

            return searchTerm + ";" + totalOrdersTerm;
        }
    }
}
