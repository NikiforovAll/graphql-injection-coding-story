namespace GraphQLInjectionCodingStory;

using HotChocolate;
using HotChocolate.Validation;
using HotChocolate.Language;
using HotChocolate.Language.Visitors;
using HotChocolate.Types.Introspection;

internal class NoIntrospectionDocumentValidatorVisitor : TypeDocumentValidatorVisitor
{
    protected override ISyntaxVisitorAction Enter(FieldNode node, IDocumentValidatorContext context)
    {
        if (node.Name.Value == IntrospectionFields.Schema || node.Name.Value == IntrospectionFields.Type)
        {
            IError error = ErrorBuilder.New().SetMessage("Introspection queries are disabled").Build();
            context.Errors.Add(error);
            return new BreakSyntaxVisitorAction();
        }

        return base.Enter(node, context);
    }
}
