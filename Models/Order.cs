namespace GraphQLInjectionCodingStory
{
    public class Order
    {
        public int Id { get; set; }

        public int? CustomerId { get; set; }

        public User Customer { get; set; }

        public string Sku { get; set; }
        
        public string Description { get; set; }

        public DateTime OrderDate { get; set; }
    }
}
