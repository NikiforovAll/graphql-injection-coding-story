namespace GraphQLInjectionCodingStory
{
    public class OrderDto
    {
        public int Id { get; set; }

        public string Sku { get; set; }
        
        public string Description { get; set; }

        public string TotalOrdersLabel { get; set; }
    }
}
