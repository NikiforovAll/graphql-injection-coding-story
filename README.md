# GraphQL Injection Story

**To read**: <https://cheatsheetseries.owasp.org/cheatsheets/GraphQL_Cheat_Sheet.html>

**Estimated reading time**: 15 min

## Story Outline

This story is focused on providing guidance for preventing GraphQL Injection flaws
in your applications.

## Story Organization

**Story Branch**: main
> `git checkout main`

Tags: #csharp, #aspnetcore, #graphql, #security
