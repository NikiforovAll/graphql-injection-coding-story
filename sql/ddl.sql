-- Table: public.Users

-- DROP TABLE public."Users";

CREATE TABLE IF NOT EXISTS public."Users"
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "Name" character varying COLLATE pg_catalog."default",
    "Password" character varying COLLATE pg_catalog."default",
    CONSTRAINT "Users_pkey" PRIMARY KEY ("Id")
);

-- Table: public.Orders

-- DROP TABLE public."Orders";

CREATE TABLE IF NOT EXISTS public."Orders"
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "UserId" bigint,
    "Sku" character varying COLLATE pg_catalog."default",
    "Description" text COLLATE pg_catalog."default",
    "Name" character varying COLLATE pg_catalog."default",
    CONSTRAINT "Orders_pkey" PRIMARY KEY ("Id"),
    CONSTRAINT "Orders_UserId_fkey" FOREIGN KEY ("UserId")
        REFERENCES public."Users" ("Id") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
