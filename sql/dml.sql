INSERT INTO public."Users"("Name", "Password")
VALUES
    ('admin', 'pa$$sw0rd'),
    ('John Doe', 'johndoe123');


INSERT INTO public."Orders"("UserId", "Name", "Sku")
VALUES
    (2, 'test-order1', 'PR-1'),
    (2, 'test-order2', 'PR-2'),
    (2, 'test-order3', 'PR-3'),
    (2, 'test-order4', 'PR-1'),
    (2, 'test-order5', 'PR-4'),
    (2, 'test-order6', 'PR-5'),
    (2, 'test-order7', 'PR-5'),
    (2, 'test-order8', 'PR-5');
